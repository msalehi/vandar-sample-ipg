package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Vandar Shop")
	})
	app.Get("/payment", func(c *fiber.Ctx) error {
		token := getPaymentToken()
		url := "https://ipg.vandar.io/v3/"
		return c.Redirect(url+token)
	})

	app.Listen(":3000")
}

func getPaymentToken() string {

	// Create the request body
	requestBody, err := json.Marshal(map[string]interface{}{
		"api_key":      "APIKEYXXXXXXXXXXXXXXXXXXXXXXX",
		"amount":       250000,
		"callback_url": "http://localhost:3000/payment/callback",
	})
	if err != nil {
		panic(err)
	}

	// Create the request
	req, err := http.NewRequest("POST", "https://ipg.vandar.io/api/v3/send", bytes.NewBuffer(requestBody))
	if err != nil {
		panic(err)
	}

	// Set the request headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer <token>")

	// Send the request and retrieve the response
	client := &http.Client{Timeout: time.Second * 10}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// Read the response body
	defer resp.Body.Close()
	var response map[string]string
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		panic(err)
	}

	// Retrieve the payment token from the response
	token := response["token"]

	return token
}
